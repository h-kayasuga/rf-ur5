#!/bin/bash

source devel_isolated/setup.bash
roslaunch ur_robot_driver ur5e_bringup.launch robot_ip:=10.10.11.10 kinematics_config:="${HOME}/.ros/robot_calibration.yaml"