#!/bin/bash

# git
echo "---init submodule---"
git submodule init
git submodule sync
git submodule update

# ros package
echo "---update ros package---"
source /opt/ros/kinetic/setup.bash
sudo apt update -qq
rosdep update
rosdep install --from-paths src --ignore-src -y