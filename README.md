# rf-ur5 #

### What is this repository for? ###
* UR5の状態をRosTopicとして取得するためのソフトウェア

### How do I get set up? ###

* rf-wsまたはrf-master-wsを参考にROSの環境構築
* スクリプトで環境構築

    ./script/setup.sh

* ビルド

    ./build.sh

### How to use? ###

* 初回はUR5内のキャリブレーションデータを取得

    ./script/run_calibration.sh

* Bring up ur5 topic

    ./launch_ur5_bringup.sh